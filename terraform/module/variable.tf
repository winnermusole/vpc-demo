# General Variables

variable "aws_region" {
    description = "Default region for provider"
    type = string
    default = "us-west-2"
}

variable "project_name" {
    description = "Name of the project"
    type = string
    default = "Yane"
}

# EC2 Variables

variable "ami-Ubuntu" {
    description = "Amazon Machine Image used for our EC2 instances | Ubuntu, 22.04 LTS, amd64 us-west-2"
    type = string
    default = "ami-03f65b8614a860c29"
}

variable "ami-AMZ" {
    description = "Amazon Machine Image used for our EC2 instances | Amazon Linux 2023 AMI 2023 us-west-2"
    type = string
    default = "ami-07dfed28fcf95241c"
}



variable "instance_type" {
    description = "EC2 instance type"
    type = string
    default = "t2.micro"
}

# Networking Variables

# VPC CIDR Block
variable "vpc_cidr" {
    description = "value"
    default = "10.1.0.0/16"
}

# public subnet cidr block
variable "public_subnet_cidr" {
    description = "CIDR block for Public Subnet"
    default = "10.1.1.0/25"
}

# private subnet cidr block
variable "private_subnet_cidr" {
    description = "CIDR block for Private Subnet"
    default = "10.1.1.128/25"
}

# availability zones list
variable "availability_zone_names" {
    description = "List AZs to be used"
    type = list(string)
    default = [ "us-west-2a", "us-west-2b" ]
}