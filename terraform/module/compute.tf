# EC2 instances
resource "aws_instance" "instance_reggae" {
    ami = var.ami-Ubuntu
    instance_type = var.instance_type
    subnet_id = "${aws_subnet.Yane_public_subnet[0].id}"
    vpc_security_group_ids = [ aws_security_group.reggae_instance.id ]
    availability_zone = "${var.availability_zone_names[0]}"
    user_data = <<-EOF
                #!/bin/bash
                sudo apt update -y
                sudo apt install nginx -y
                sudo systemctl enable nginx
                EOF
    tags = {
        Name = "EC2 Reggae Instance - NGINX"
    }
}

resource "aws_instance" "instance_ndombolo" {
    ami = var.ami-Ubuntu
    instance_type = var.instance_type
    subnet_id = "${aws_subnet.Yane_public_subnet[1].id}"
    availability_zone = "${var.availability_zone_names[1]}"
    user_data = <<-EOF
                #!/bin/bash
                sudo apt update -y
                sudo apt install -y httpd
                sudo systemctl start httpd
                sudo systemct enable httpd
                echo "<h1>EC2 Instance: Papa Wemba and Koffi Olomide ⚡️</h1>" > /var/www/html/index.html
                EOF
    tags = {
        Name = "EC2 Ndombolo Instance"
    }
}