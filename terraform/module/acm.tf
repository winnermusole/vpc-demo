locals {
  my_domain = "traduzer.pro"
  alt_name = ["*.traduzer.pro"]
}

resource "aws_acm_certificate" "yane_domain" {
    domain_name = local.my_domain
    subject_alternative_names = local.alt_name
    validation_method = "DNS"
    tags = {
        Environment = "test"
    }

    lifecycle {
        create_before_destroy = true
    }
}

data "aws_route53_zone" "yane_domain" {
    name = local.my_domain
    private_zone = false
}

resource "aws_route53_record" "yane_domain" {
    for_each = {
      for dvo in aws_acm_certificate.yane_domain.domain_validation_options : dvo.domain_name => {
        name   = dvo.resource_record_name
        record = dvo.resource_record_value
        type   = dvo.resource_record_type
      } 
    }
    allow_overwrite = true
    name = each.value.name
    records = [each.value.record]
    ttl = 60
    type = each.value.type
    zone_id = data.aws_route53_zone.yane_domain.zone_id
}

resource "aws_acm_certificate_validation" "yane_domain" {
    certificate_arn = aws_acm_certificate.yane_domain.arn
    validation_record_fqdns = [for record in aws_route53_record.yane_domain : record.fqdn]
}