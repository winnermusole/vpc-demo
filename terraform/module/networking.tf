# VPC
resource "aws_vpc" "Yane" {
  cidr_block = var.vpc_cidr
  instance_tenancy = "default"
  enable_dns_hostnames = true
  
  tags = {
    Name = "Yane VPC"
  }
}

# Public Subnet
resource "aws_subnet" "Yane_public_subnet" {
    count = "${length(var.availability_zone_names)}"
    cidr_block = "10.1.${1+count.index}.0/26"
    vpc_id = aws_vpc.Yane.id
    map_public_ip_on_launch = true
    availability_zone = "${var.availability_zone_names[count.index]}"

    tags = {
      Name = "Yane Public-Subnet"
    }
}

# Private subnet
resource "aws_subnet" "Yane_private_subnet" {
    count = "${length(var.availability_zone_names)}"
    cidr_block = "10.1.${3+count.index}.0/26"
    vpc_id = aws_vpc.Yane.id
    map_public_ip_on_launch = false
    availability_zone = "${var.availability_zone_names[count.index]}"

    tags = {
        Name = "Yane Private-Subnet"
    }
}

# Security group
resource "aws_security_group" "reggae_instance" {
    name = "Reggae Instance SG"
    vpc_id = aws_vpc.Yane.id

    ingress {
        from_port = 80
        to_port = 80
        protocol = "tcp"
        security_groups = [ aws_security_group.Yane_ALB.id ]
    }

    ingress {
        from_port = 22
        to_port = 22
        protocol = "tcp"
        cidr_blocks = [ "0.0.0.0/0" ]
    }

    egress {
        from_port = 0
        to_port = 0
        protocol = "-1"
        cidr_blocks = ["0.0.0.0/0"]
    }

}

# Internet gateway for internet access with public subnet
resource "aws_internet_gateway" "Yane" {
    tags = {
      Name = "Yane IGW"
    }
    vpc_id = aws_vpc.Yane.id
}

# route table for public subnet: regulate network traffics
resource "aws_route_table" "Yane_PUB_RT" {
  vpc_id = aws_vpc.Yane.id

  route {
      cidr_block = "0.0.0.0/0"
      gateway_id = aws_internet_gateway.Yane.id
  }

  tags = {
      Name = "VioTu_PUB_RT"
  }
}

# route table subnet association
resource "aws_route_table_association" "Yane" {
  subnet_id = "${aws_subnet.Yane_public_subnet[0].id}"
  route_table_id = aws_route_table.Yane_PUB_RT.id
}

# Load Balancer

# ALB Security Group
resource "aws_security_group" "Yane_ALB" {
    name = "Yane_ALB_Security_Group"
    vpc_id = aws_vpc.Yane.id

    # Allow HTTP into ALB
    ingress {
        from_port = 80
        to_port = 80
        protocol = "TCP"
        cidr_blocks = ["0.0.0.0/0"]
    }
    
    # Allow HTTPS into ALB
    ingress {
        from_port   = 443
        to_port     = 443
        protocol    = "tcp"
        cidr_blocks = ["0.0.0.0/0"]
    }

    # Allow outbound rules for ALB
    egress {
        from_port = 0
        to_port = 0
        protocol = "-1"
        cidr_blocks = [ "0.0.0.0/0" ]
    }
}

# Application Load Balancer (ALB)
resource "aws_lb" "Yane" {
    name = "Yane-ALB"
    load_balancer_type = "application"
    subnets = [ aws_subnet.Yane_public_subnet[0].id, aws_subnet.Yane_public_subnet[1].id ]
    security_groups = [ aws_security_group.Yane_ALB.id ]

}

# Target group
resource "aws_lb_target_group" "Yane" {
    name = "Yane-Target-Group"
    port = 80
    protocol = "HTTP"
    target_type = "instance"
    vpc_id = aws_vpc.Yane.id

    health_check {
        path                = "/"
        protocol            = "HTTP"
        matcher             = "200"
        interval            = 15
        timeout             = 3
        healthy_threshold   = 2
        unhealthy_threshold = 2
    }
}

# EC2 instance attached to target group
resource "aws_lb_target_group_attachment" "Yane" {
    target_group_arn = aws_lb_target_group.Yane.arn
    target_id = aws_instance.instance_reggae.id
    port = 80
}

# ALB HTTP Listener to redirect to https
resource "aws_lb_listener" "http" {
    load_balancer_arn = aws_lb.Yane.arn
    port = 80
    protocol = "HTTP"

    default_action {
        type = "forward"
        target_group_arn = "${aws_lb_target_group.Yane.arn}"
    }
    # default_action {
    #   type = "redirect"
    #   redirect {
    #       port = 443
    #       protocol = "HTTPS"
    #       status_code = "HTTP_301"
    #   }
    # }
}

# # ALB HTTPS Listener
resource "aws_lb_listener" "https" {
    depends_on = [
        aws_acm_certificate.yane_domain,
        aws_route53_record.yane_domain,
        aws_acm_certificate_validation.yane_domain
    ]
    load_balancer_arn = aws_lb.Yane.arn
    port = 443
    protocol = "HTTPS"
    certificate_arn = aws_acm_certificate_validation.yane_domain.certificate_arn
    default_action {
        type = "forward"
        target_group_arn = "${aws_lb_target_group.Yane.arn}"
    }
}

